# Copyright 2007 Bryan Østergaard
# Copyright 2008 Richard Brown
# Copyright 2009 Bo Ørsted Andresen
# Copyright 2010-2017 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ release=v${PV} suffix=tar.xz ]

SUMMARY="System call trace utility"

HOMEPAGE="https://strace.io"
# Let's encrypt cert is outdated, so fetch from github for now
#DOWNLOADS="${HOMEPAGE}/files/${PV}/${PNV}.tar.xz"

REMOTE_IDS="freshcode:${PN}"

UPSTREAM_RELEASE_NOTES="${HOMEPAGE}/files/${PV}/README-${PV}.txt"

LICENCES="
    GPL-2 [[ note = [ test suite, bundled linux kernel headers ] ]]
    LGPL-2.1
"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS="
    selinux

    (
        providers:
            elfutils-unwind [[ description = [ Use libdw from elfutils as unwinder ] ]]
            libunwind
    ) [[ number-selected = exactly-one ]]
"

# sydbox makes the tests fail even with all the disabled stuff below and
# RESTRICT="userpriv" due to the usage of ptrace. Last checked: 4.11
RESTRICT="test"

DEPENDENCIES="
    build:
        sys-devel/binutils
    build+run:
        providers:elfutils-unwind? ( dev-util/elfutils )
        providers:libunwind? ( dev-libs/libunwind )
        selinux? ( security/libselinux )
    test:
        sys-apps/busybox
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    CC_FOR_BUILD=$(exhost --build)-cc
    READELF=/usr/$(exhost --build)/$(exhost --target)/bin/readelf.binutils
    --enable-stacktrace
    --enable-bundled=yes
)

if [[ $(exhost --target) == "aarch64"* ]] || [[ $(exhost --target) == *-musl* ]] ; then
    # - mpers is not supported by armv8
    # - disable mpers on musl due to https://github.com/strace/strace/issues/133
    DEFAULT_SRC_CONFIGURE_PARAMS+=(
        --disable-mpers
    )
else
    DEFAULT_SRC_CONFIGURE_PARAMS+=(
        --enable-mpers=yes
    )
fi

DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    'providers:elfutils-unwind libdw'
    'providers:libunwind'
    'selinux libselinux'
)

DEFAULT_SRC_COMPILE_PARAMS=( AR=${AR} )

src_test() {
    esandbox disable
    esandbox disable_exec
    emake -j1 check
    esandbox enable
    esandbox enable_exec
}

