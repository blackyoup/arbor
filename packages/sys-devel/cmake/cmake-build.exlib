# Copyright 2008 Wulf Krueger <philantrop@exherbo.org>
# Copyright 2008, 2009, 2010 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2014-2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'cmake-2.6.0.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation.

require cmake [ out_of_source=false ]
require bash-completion elisp [ with_opt=true source_directory=Auxiliary ] flag-o-matic
require freedesktop-desktop freedesktop-mime gtk-icon-cache
require option-renames [ renames=[ 'qt5 gui' ] ]

export_exlib_phases src_prepare src_configure src_compile src_test src_install \
                    pkg_postinst pkg_postrm

SUMMARY="Cross platform Make"
HOMEPAGE="https://www.cmake.org"
DOWNLOADS="${HOMEPAGE}/files/v$(ever range 1-2)/${PNV}.tar.gz"

LICENCES="
    BSD-3
    gui? ( LGPL-3 )
"
SLOT="0"
MYOPTIONS="
    bootstrap [[ description = [ Use the internal jsoncpp (it needs cmake to build itself) ] ]]
    doc       [[ description = [ Install HTML documentation and man pages ] ]]
    gui       [[ description = [ Build a Qt based GUI to configure projects ] ]]
    ncurses
    qthelp    [[ description = [ Build cmake's docs as Qt Help file (.qch) ] ]]
    vim-syntax

    ( parts: binaries data development documentation )

    gui? (
        ( providers: qt5 qt6 ) [[
            number-selected = exactly-one
            *requires = gui
        ]]
    )
"

DEPENDENCIES="
    build:
        virtual/pkg-config
       !bootstrap? ( sys-devel/cmake[>=3.13] )
        doc? ( dev-python/Sphinx[>=1.6.5] )
    build+run:
        app-arch/libarchive[>=3.3.3] [[ note = [ First version with zstd support ] ]]
        app-crypt/rhash
        dev-libs/expat[>=2.0.1]
        dev-libs/libuv[>=1.28.0]
        net-misc/curl[>=7.16.4][ssl(+)]
        ncurses? ( sys-libs/ncurses )
        gui? (
            providers:qt5? ( x11-libs/qtbase:5 )
            providers:qt6? ( x11-libs/qtbase:6 )
        )
        qthelp? (
            dev-lang/python:*
            x11-libs/qttools:5
        )
"
# TODO: Versions >= 3.5.0 bundle KWIML (Kitware Information Macro Library,
# https://github.com/Kitware/KWIML ). It's currently not used elsewhere,
# handled a bit differently than other bundled dependencies and, most
# importantly, there's no released tarball. So for now we stick with the
# bundled version.

if ever at_least 3.25.0-rc1 ; then
    DEPENDENCIES+="
        build+run:
           !bootstrap? ( dev-libs/jsoncpp:=[>=1.6.0] )
        "
else
    DEPENDENCIES+="
        build+run:
           !bootstrap? ( dev-libs/jsoncpp:=[>=1.4.1] )
    "
fi

BASH_COMPLETIONS=(
    Auxiliary/bash-completion/cmake
    'Auxiliary/bash-completion/cpack cpack'
    'Auxiliary/bash-completion/ctest ctest'
)

# TODO: Tests are broken because of the multiarch migration. One problem is,
# that we probably need to figure out a way to pass our arch prefixed tools to
# the relevant tests. Additionally cmake isn't really verbose about what's
# wrong. So better disable them for now until someone finds time to fix them
# (heirecka).
RESTRICT="test"

VIMFILE="${PN}.vim"

DEFAULT_SRC_TEST_PARAMS=( ARGS=--verbose )

cmake-build_src_prepare() {
    default

    # Don't install bash-completions, use bash-completion.exlib
    edo sed -e '/^install(FILES cmake cpack/d' \
            -i Auxiliary/bash-completion/CMakeLists.txt

    # Teach it about our suffixed qcollectiongenerator
    edo sed \
        -e "/NAMES /s/qcollectiongenerator/qcollectiongenerator-qt5 &/" \
        -i Utilities/Sphinx/CMakeLists.txt
}

cmake-build_src_configure() {
    local host=$(exhost --target)
    local bootstrap_params=(
        --parallel=${EXJOBS:-1}
        --$(option gui || echo "no-")qt-gui
        --system-curl
        --system-expat
        --system-libarchive
        --system-librhash
        --system-zlib
        --system-libuv
        --prefix=/usr/${host}
        --docdir=/../share/doc/${PNVR}
        --datadir=/../share/${PN}
        --mandir=/../share/man
        --xdgdatadir=/../share
        --verbose
        $(option doc && echo "--sphinx-info")
        $(option doc && echo "--sphinx-man --sphinx-html")
        $(option qthelp && echo "--sphinx-qthelp")
        $(option bootstrap --no-system-jsoncpp --system-jsoncpp)

        # Only relevant when using the bundled libarchive
        --no-system-bzip2
        --no-system-liblzma
        --no-system-zstd
    )

    local cmakeargs=(
        -DCMAKE_DATA_DIR:PATH=/../share/${PN}
        -DCMAKE_DOC_DIR:PATH=/../share/doc/${PNVR}
        -DCMAKE_MAN_DIR:PATH=/../share/man
        -DCMAKE_XDGDATA_DIR:PATH=/../share
        -DCMAKE_USE_SYSTEM_CURL:BOOL=TRUE
        -DCMAKE_USE_SYSTEM_EXPAT:BOOL=TRUE
        -DCMAKE_USE_SYSTEM_LIBARCHIVE:BOOL=TRUE
        -DCMAKE_USE_SYSTEM_LIBRHASH:BOOL=TRUE
        -DCMAKE_USE_SYSTEM_LIBUV:BOOL=TRUE
        -DCMAKE_USE_SYSTEM_ZLIB:BOOL=TRUE

        # Only relevant when using the bundled libarchive
        -DCMAKE_USE_SYSTEM_BZIP2:BOOL=FALSE
        -DCMAKE_USE_SYSTEM_LIBLZMA:BOOL=FALSE

        $(cmake_option doc SPHINX_HTML)
        $(cmake_option doc SPHINX_INFO)
        $(cmake_option doc SPHINX_MAN)
        $(cmake_option qthelp SPHINX_QTHELP)

        $(cmake_build ncurses CursesDialog)
        $(cmake_build gui QtDialog)
        $(option gui && echo "-DCMake_GUI_DISTRIBUTE_WITH_Qt_LGPL=3")
        $(expecting_tests "-DBUILD_TESTING:BOOL=ON" "-DBUILD_TESTING:BOOL=OFF")
    )

    if option bootstrap; then
        # Keep the bootstrap script from configuring the real cmake
        edo sed -e '/"${cmake_bootstrap_dir}\/cmake"/s/^/# /' \
                -i bootstrap

        # TODO(tridactyla): There are still some issues with cross-compiling to a non-native platform
        # related to generating documentation (it tries to run executables it just built).
        edo env                                                                     \
            CC="$(exhost --build)-cc"                                               \
            CXX="$(exhost --build)-c++"                                             \
            CFLAGS="$(print-build-flags CFLAGS)"                                    \
            CXXFLAGS="$(print-build-flags CXXFLAGS)"                                \
            LDFLAGS="$(print-build-flags LDFLAGS)"                                  \
        ./bootstrap "${bootstrap_params[@]}"

        CMAKE_BINARY=${PWD}/Bootstrap.cmk/cmake

        cmakeargs+=(
            -DCMAKE_C_IMPLICIT_INCLUDE_DIRECTORIES:PATH=/usr/$(exhost --target)/include
            -DCMAKE_CXX_IMPLICIT_INCLUDE_DIRECTORIES:PATH=/usr/$(exhost --target)/include
        )
    fi

    cmakeargs+=(
        # Only relevant when using the bundled libarchive
        -DCMAKE_USE_SYSTEM_ZSTD:BOOL=FALSE
        -DCMake_QT_MAJOR_VERSION=$(option providers:qt6 && echo "6" || echo "5")
        -DCMake_TEST_NO_NETWORK:BOOL=TRUE
        -DSPHINX_LATEXPDF:BOOL=FALSE
        $(cmake_option !bootstrap CMAKE_USE_SYSTEM_JSONCPP)
    )

    ecmake "${cmakeargs[@]}"
}

cmake-build_src_compile() {
    default
    elisp_src_compile
}

cmake-build_src_test() {
    edo bin/ctest -V -E "CTestTestFailedSubmit|Qt4And5AutomocReverse"
}

cmake-build_src_install() {
    default

    bash-completion_src_install
    elisp_src_install

    option emacs|| edo rm -r "${IMAGE}"/usr/share/emacs
    option vim-syntax || edo rm -r "${IMAGE}"/usr/share/vim

    if option qthelp ; then
        # Uses DOCDIR like the other docs but for easier consumption we move
        # where Qt can easily find it
        edo mkdir "${IMAGE}"/usr/share/doc/qt5
        edo mv "${IMAGE}"/usr/share/doc/{${PNVR}/CMake.qch,qt5}
    fi

    expart binaries /usr/$(exhost --target)/bin
    expart data /usr/share
    expart documentation /usr/share/{doc,info,man}
    expart development /usr/share/cmake/include /usr/share/aclocal
}

cmake-build_pkg_postinst() {
    if option gui ; then
        freedesktop-desktop_pkg_postinst
        freedesktop-mime_pkg_postinst
        gtk-icon-cache_pkg_postinst
    fi
}

cmake-build_pkg_postrm() {
    if option gui ; then
        freedesktop-desktop_pkg_postrm
        freedesktop-mime_pkg_postrm
        gtk-icon-cache_pkg_postrm
    fi
}

