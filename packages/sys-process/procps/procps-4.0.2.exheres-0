# Copyright 2007 Bryan Østergaard
# Distributed under the terms of the GNU General Public License v2

MY_PN=${PN}-ng
require sourceforge [ suffix=tar.xz ]
require alternatives
require option-renames [ renames=[ 'systemd providers:systemd' ] ]

HOMEPAGE+=" https://gitlab.com/procps-ng/procps"
SUMMARY="Utilities for process information including ps, top and kill"

LICENCES="GPL-2 LGPL-2"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS="
    ( providers: elogind systemd ) [[
        *description = [ Session tracking provider ]
        number-selected = at-most-one
    ]]

    ( parts: binaries data development documentation libraries )
    ( libc: musl )
    ( linguas: de fr pl pt_BR sv uk vi zh_CN )
"

DEPENDENCIES="
    build:
        sys-devel/gettext
        virtual/pkg-config
    build+run:
        sys-libs/ncurses
        providers:elogind? ( sys-auth/elogind[>=206] )
        providers:systemd? ( sys-apps/systemd[>=206] )
        !sys-apps/sysvinit-tools[<2.88-r6] [[
            description = [ procps now provides pidof ]
            resolution = upgrade-blocked-before
        ]]
        !sys-apps/coreutils[<8.23-r3]  [[
            description = [ procps now provides uptime ]
            resolution = upgrade-blocked-before
        ]]
        !sys-apps/coreutils[<8.23-r4]  [[
            description = [ conflicts with uptime alternatives ]
            resolution = uninstall-blocked-after
        ]]
    test:
        dev-util/dejagnu
"

WORK=${WORKBASE}/${MY_PN}-${PV}

# NOTE: po4a is only needed for the update-po target, which isn't invoked with
# tarballs already containing translated man pages.

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/0001-testsuite-Remove-problematic-free-with-commit-test.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    ac_cv_func_malloc_0_nonnull=yes
    ac_cv_func_realloc_0_nonnull=yes

    --enable-modern-top
    --enable-nls
    --enable-pidof
    --enable-sigwinch
    --enable-watch8bit
    --disable-harden-flags
    --disable-libselinux
    --disable-{s,}kill
    --disable-static
    --with-ncurses
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    'providers:elogind elogind'
    'providers:systemd systemd'
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    '!libc:musl w'
)

src_install() {
    default

    # remove empty directories, last checked: 4.0.0
    edo find "${IMAGE}"/usr/share/man -type d -empty -delete

    expart binaries /usr/$(exhost --target)/{,s}bin
    expart data /usr/share
    expart documentation /usr/share/{doc,man}
    expart libraries /usr/$(exhost --target)/lib
    expart development /usr/$(exhost --target)/{include,lib/pkgconfig}

    if option parts:binaries;then
        alternatives_for uptime ${PN} 2000  \
            /usr/$(exhost --target)/bin/uptime  uptime.${PN}
    fi
    if option parts:documentation;then
        alternatives_for uptime ${PN} 2000  \
            /usr/share/man/man1/uptime.1        uptime.${PN}.1
    fi
}

