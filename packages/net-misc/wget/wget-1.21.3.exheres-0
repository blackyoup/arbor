# Copyright 2007 Bryan Østergaard
# Distributed under the terms of the GNU General Public License v2

require alternatives
require gnu [ suffix=tar.gz ]
require option-renames [ renames=[ 'gnutls providers:gnutls' ] ]

SUMMARY="Wget is a package for retrieving files using HTTP, HTTPS and FTP"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS="
    ares [[ description = [ Enable support for C-Ares DNS lookup ] ]]
    idn [[ description = [ Enable support for internationalized domain names ] ]]
    ( providers: gnutls libressl openssl ) [[ number-selected = exactly-one ]]
    ( linguas: be bg ca cs da de el en_GB eo es et eu fi fr ga gl he hr hu id it ja lt nb nl pl pt
               pt_BR ro ru sk sl sr sv tr uk vi zh_CN zh_TW )
"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.19.3]
        virtual/pkg-config[>=0.9.0]
    build+run:
        dev-libs/pcre2
        sys-apps/util-linux [[ note = [ for libuuid ] ]]
        sys-libs/zlib
        ares? ( net-dns/c-ares )
        idn? ( net-dns/libidn2:=[>=0.14.0] )
        providers:gnutls? (
            dev-libs/gmp:=
            dev-libs/gnutls
            dev-libs/libtasn1
            dev-libs/nettle:=
        )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
    test:
        dev-perl/HTTP-Daemon
        dev-perl/IO-Socket-SSL
"

DEFAULT_SRC_INSTALL_EXTRA_DOCS=( MAILING-LIST doc/sample.wgetrc )

src_configure() {
    local myconf=(
        gt_cv_func_gnugettext{1,2}_libc=yes
        --enable-ipv6
        --enable-nls
        --enable-ntlm
        --enable-pcre2
        --enable-xattr
        --disable-assert
        --disable-debug
        --disable-manywarnings
        --disable-pcre
        --disable-rpath
        --with-libuuid
        --with-ssl=$(option providers:gnutls gnutls openssl)
        --with-zlib
        --without-libpsl
        --without-metalink
        $(option_enable idn iri)
        $(option_with ares cares)
    )

    econf ${myconf[@]}
}

src_test() {
    unset http_proxy https_proxy ftp_proxy

    # avoid tests failing with
    #     ERROR: cannot verify localhost's certificate, issued by ‘O=GNU,OU=Wget,CN=GNU Wget’:
    #       format error in certificate's notBefore field
    edo pushd testenv/certs
    edo ./make_ca.sh
    edo popd

    default
}

src_install() {
    default

    # NOTE(somasis): remove gettext remenants on musl
    edo rm -rf "${IMAGE}"/usr/$(exhost --target)/lib

    alternatives_for wget gnu 1000 \
        /usr/$(exhost --target)/bin/wget gnu${PN} \
        /usr/share/man/man1/wget.1       gnuwget.1
}

