# Copyright 2016 Julian Ospald <hasufell@posteo.de>
# Copyright 2023 Maxime Sorin <maxime.sorin@clever-cloud.com>
# Distributed under the terms of the GNU General Public License v2

require alternatives github [ user="elixir-lang" tag=v${PV} ] utf8-locale providers

export_exlib_phases pkg_setup src_compile src_install

SUMMARY="Elixir programming language"
HOMEPAGE="https://elixir-lang.org"

LICENCES="Apache-2.0 EPL-1.1"
SLOT="$(ever range 1-2)"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        !dev-lang/elixir:0 [[
            description = [ Elixir is now slotted ]
            resolution = uninstall-blocked-before
        ]]
"

DEFAULT_SRC_COMPILE_PARAMS=(
    Q=""
)

DEFAULT_SRC_INSTALL_PARAMS=(
    PREFIX="/usr/$(exhost --target)"
    SHARE_PREFIX="/usr/share"
)

elixir_pkg_setup() {
    require_utf8_locale
}

elixir_src_compile() {
    providers_set "erlang $(erlang_get_abi)"

    default
}

elixir_src_install() {
    default

    local alternatives=()

    # By default, binaries are symbolic links to ../lib/elixir
    # The alternatives will be on the ../lib/elixir folder
    alternatives+=(
        "/usr/$(exhost --target)/lib/${PN}" "${PN}-${SLOT}"
    )

    # Alternatives for binaries and man pages
    edo pushd ${IMAGE}/usr/$(exhost --target)/bin

    for file in *; do
        # Remove default symbolic links
        edo rm ${file}

        alternatives+=(
            "/usr/$(exhost --target)/bin/${file}" "/usr/$(exhost --target)/lib/${PN}-${SLOT}/bin/${file}"
            "/usr/share/man/man1/${file}.1" "${file}-${SLOT}"
        )
    done

    edo popd

    # Remove empty bin directory
    edo rmdir ${IMAGE}/usr/$(exhost --target)/bin

    alternatives_for ${PN} ${SLOT} ${SLOT} "${alternatives[@]}"
}

RESTRICT="test"
