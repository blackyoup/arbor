Upstream: yes, cherry-picked from stable/2.0
http://git.savannah.gnu.org/cgit/guile.git/commit/?h=stable-2.0&id=245608911698adb3472803856019bdd5670b6614

From 1e257969f4fcce193ba938b19ff64c739d2980c3 Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?Ludovic=20Court=C3=A8s?= <ludo@gnu.org>
Date: Tue, 11 Oct 2016 10:14:26 +0200
Subject: [PATCH] Remove 'umask' calls from 'mkdir'.

Fixes <http://bugs.gnu.org/24659>.

* libguile/filesys.c (SCM_DEFINE): Remove calls to 'umask' when MODE is
unbound; instead, use 0777 as the mode.  Update docstring to clarify
this.
* doc/ref/posix.texi (File System): Adjust accordingly.
* NEWS: Mention it.
---
 doc/ref/posix.texi |  7 ++++---
 libguile/filesys.c | 22 ++++++++--------------
 2 files changed, 12 insertions(+), 17 deletions(-)

diff --git a/doc/ref/posix.texi b/doc/ref/posix.texi
index 0a7e3423b..868a64d22 100644
--- a/doc/ref/posix.texi
+++ b/doc/ref/posix.texi
@@ -815,9 +815,10 @@ Create a symbolic link named @var{newpath} with the value (i.e., pointing to)
 @deffn {Scheme Procedure} mkdir path [mode]
 @deffnx {C Function} scm_mkdir (path, mode)
 Create a new directory named by @var{path}.  If @var{mode} is omitted
-then the permissions of the directory file are set using the current
-umask (@pxref{Processes}).  Otherwise they are set to the decimal
-value specified with @var{mode}.  The return value is unspecified.
+then the permissions of the directory are set to @code{#o777}
+masked with the current umask (@pxref{Processes, @code{umask}}).
+Otherwise they are set to the value specified with @var{mode}.
+The return value is unspecified.
 @end deffn
 
 @deffn {Scheme Procedure} rmdir path
diff --git a/libguile/filesys.c b/libguile/filesys.c
index c76f407c2..ca4b60650 100644
--- a/libguile/filesys.c
+++ b/libguile/filesys.c
@@ -792,24 +792,18 @@ SCM_DEFINE (scm_delete_file, "delete-file", 1, 0, 0,
 SCM_DEFINE (scm_mkdir, "mkdir", 1, 1, 0,
             (SCM path, SCM mode),
 	    "Create a new directory named by @var{path}.  If @var{mode} is omitted\n"
-	    "then the permissions of the directory file are set using the current\n"
-	    "umask.  Otherwise they are set to the decimal value specified with\n"
-	    "@var{mode}.  The return value is unspecified.")
+	    "then the permissions of the directory are set to @code{#o777}\n"
+	    "masked with the current umask (@pxref{Processes, @code{umask}}).\n"
+	    "Otherwise they are set to the value specified with @var{mode}.\n"
+	    "The return value is unspecified.")
 #define FUNC_NAME s_scm_mkdir
 {
   int rv;
-  mode_t mask;
+  mode_t c_mode;
 
-  if (SCM_UNBNDP (mode))
-    {
-      mask = umask (0);
-      umask (mask);
-      STRING_SYSCALL (path, c_path, rv = mkdir (c_path, 0777 ^ mask));
-    }
-  else
-    {
-      STRING_SYSCALL (path, c_path, rv = mkdir (c_path, scm_to_uint (mode)));
-    }
+  c_mode = SCM_UNBNDP (mode) ? 0777 : scm_to_uint (mode);
+
+  STRING_SYSCALL (path, c_path, rv = mkdir (c_path, c_mode));
   if (rv != 0)
     SCM_SYSERROR;
   return SCM_UNSPECIFIED;
-- 
2.11.0

